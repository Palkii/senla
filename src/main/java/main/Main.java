package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import context.MainInitializer;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        MainInitializer initializer = new MainInitializer();
        initializer.init();

        SpringApplication.run(Main.class, args);
    }

}
