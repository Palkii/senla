package Repositories;

import Storage.AbstractDataStorage;
import Storage.CustomerStorage;
import a.repositories.CustomerRepository;
import entity.Customer;

public class CustomerRepositoryImpl extends AbstractRepositoryImpl<Customer> implements CustomerRepository {

    private static CustomerRepository instance;

    private CustomerRepositoryImpl(AbstractDataStorage<Customer> storage){
        super(storage);
    }

    public static CustomerRepository getInstance() {
        if(instance == null){
            instance = new CustomerRepositoryImpl(CustomerStorage.getInstance());
        }
        return instance;
    }
}
