package Repositories;

import Storage.AbstractDataStorage;
import Storage.ServiceStorage;
import a.repositories.ServicesRepository;
import entity.Service;

public class ServiceRepositoryImpl extends AbstractRepositoryImpl<Service> implements ServicesRepository {

    private static ServicesRepository instance;

    private ServiceRepositoryImpl(AbstractDataStorage<Service> storage){
        super(storage);
    }

    public static ServicesRepository getInstance(){
        if(instance == null){
            instance = new ServiceRepositoryImpl(ServiceStorage.getInstance());
        }
        return instance;
    }

}
