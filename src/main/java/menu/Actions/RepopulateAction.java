package menu.Actions;

import context.Context;
import entity.Apartments;
import entity.enums.Status;
import menu.Action;
import org.springframework.stereotype.Component;

@Component
public class RepopulateAction implements Action {

    @Override
    public void execute(){

        int id;
        System.out.print("Введите номер апартамента в который хотите заселить: ");
        id = Context.key.nextInt();
        for (Apartments apartments : Context.getInstance().apartments) {
            if (apartments.getRoomId() == id && apartments.getStatus() == Status.Occupied){
                System.out.println("Занято.");
                return;
            }
            else
                if(apartments.getRoomId() == id && apartments.getStatus() == Status.Service){
                System.out.print("На ремонте.");
                return;
            }
            else
            if (apartments.getRoomId() == id && apartments.getStatus() == Status.Free){
                System.out.println("Заселение прошло успешно.");
                apartments.setStatus(Status.Occupied);
                return;
            }

        }


    }

}
