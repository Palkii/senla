package menu.Actions;

import context.Context;
import entity.Apartments;
import entity.enums.Status;
import menu.Action;

public class EvictAction implements Action {

    @Override
    public void execute(){
        int id;
        System.out.print("Введите номер апартамента из который хотите выселить: ");
        id = Context.key.nextInt();
        for (Apartments apartments : Context.getInstance().apartments) {
            if (apartments.getRoomId() == id && apartments.getStatus() == Status.Occupied){
                System.out.println("Выселение прошло успешно.");
                apartments.setStatus(Status.Free);
                return;
            }
            else
            if(apartments.getRoomId() == id && apartments.getStatus() == Status.Service){
                System.out.print("На ремонте.");
                return;
            }
            else
            if (apartments.getRoomId() == id && apartments.getStatus() == Status.Free){
                System.out.println("этот апартамент свободен.");
                return;
            }

        }
    }

}
