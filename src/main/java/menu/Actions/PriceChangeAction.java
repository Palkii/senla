package menu.Actions;

import context.Context;
import entity.Apartments;
import entity.Service;
import menu.Action;

public class PriceChangeAction implements Action {

    @Override
    public void execute(){
        System.out.print("Изменить цену: " +
                "1. Апартамента" +
                "2. Услуги");
        int key = Context.key.nextInt();


        if (key == 1){
            System.out.print("Введите ID апартамента: ");
            int idApart = Context.key.nextInt();
            for (Apartments apartments : Context.getInstance().apartments){
                if (apartments.getRoomId() == idApart){
                    System.out.print("Новая цена: ");
                    int price = Context.key.nextInt();
                    apartments.setPrice(price);
                }
            }
        }
        if (key == 2){
            System.out.print("Введите ID услуги: ");
            int idService = Context.key.nextInt();
            for (Service service : Context.getInstance().services){
                if (service.getId() == idService){
                    System.out.print("Новая цена: ");
                    int price = Context.key.nextInt();
                    service.setPrices(price);
                }
            }
        }

    }

}
