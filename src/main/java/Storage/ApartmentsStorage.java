package Storage;

import entity.Apartments;

public class ApartmentsStorage extends AbstractDataStorage<Apartments> {

    private static ApartmentsStorage instance;

    private ApartmentsStorage() {

    }

    public static ApartmentsStorage getInstance() {
        if (instance == null){
            instance = new ApartmentsStorage();
        }
        return instance;
    }
}
