package Storage;

import entity.Service;

public class ServiceStorage extends AbstractDataStorage<Service>{

    private static ServiceStorage instance;

    private ServiceStorage() {

    }

    public static ServiceStorage getInstance(){
        if(instance == null){
            instance = new ServiceStorage();
        }
        return instance;
    }
}
