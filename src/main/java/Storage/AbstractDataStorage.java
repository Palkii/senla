package Storage;

import entity.BaseEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDataStorage<E extends BaseEntity> {

    private Long entityId = 0L;
    private List<E> entities = new ArrayList<>();

    private Long generateId() {
        return entityId++;
    }

    public E createEntity(E entity) {
        entity.setId(generateId());
        entities.add(entity);

        return entity;
    }

    public List<E> getEntities(){
        return entities;
    }
}
