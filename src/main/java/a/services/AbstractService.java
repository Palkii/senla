package a.services;

import entity.BaseEntity;

public interface AbstractService<T extends BaseEntity> {

 Long create(T entity);

 void delete(Long id);

 void update(T entity);

 T get(Long id);

}
