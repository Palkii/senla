package Controller;

import Services.HotelServiceImpl;
import a.services.HotelService;
import entity.Apartments;

public class HotelController extends AbstractController<Apartments, HotelService>{
    private static HotelController instance;

    private HotelController() {

        super(HotelServiceImpl.getInstance());
    }

    public static HotelController getInstance(){
        if(instance == null) {
            instance = new HotelController();
        }
        return instance;
    }

}
