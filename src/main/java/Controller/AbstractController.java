package Controller;

import a.services.AbstractService;
import entity.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public abstract class AbstractController<E extends BaseEntity, S extends AbstractService<E>> {

    protected S service;

    @Autowired
    protected AbstractController(S service){
        this.service = service;
    }

    public Long create(E entity) {
        return service.create(entity);
    }

    public void delete(Long id){
        service.delete(id);
    }

    public void update(E entity){
        service.update(entity);
    }

    public E get(Long id) {
        return service.get(id);
    }
}
