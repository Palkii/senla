package Services;

import Repositories.CustomerRepositoryImpl;
import a.repositories.CustomerRepository;
import a.services.CustomerService;
import entity.Customer;

public class CustomerServiceImpl extends AbstractServiceImpl<Customer, CustomerRepository> implements CustomerService {

    private static CustomerServiceImpl instance;

    private CustomerServiceImpl() {
        super(CustomerRepositoryImpl.getInstance());
    }

    public static CustomerService getInstance() {
        if(instance == null){
            instance = new CustomerServiceImpl();
        }
        return instance;
    }
}
