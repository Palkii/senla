package Services;

import a.repositories.AbstractRepository;
import a.services.AbstractService;
import entity.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract class AbstractServiceImpl<E extends BaseEntity, R extends AbstractRepository<E>> implements AbstractService<E> {

    protected R repository;

    @Autowired
    protected AbstractServiceImpl(R repository){
        this.repository = repository;
    }

    @Override
    public Long create(E entity){
        return repository.create(entity);
    }

    @Override
    public void delete(Long id){
        repository.delete(id);
    }

    @Override
    public void update(E entity) {
        repository.update(entity);
    }

    @Override
    public E get(Long id){
        return repository.get(id).orElseThrow(IllegalArgumentException::new);
    }

}
